package message

// Message describes the format of messages being sent to the frontend
type MessageFrontend struct {
	Type   string      `json:"type"`
	Status string      `json:"status"`
	Value  interface{} `json:"value"`
}

/*
MessageValueQuery describes outgoing messages from the query service
*/
type MessageValueQuery struct {
	QueryID string      `json:"queryID"`
	Result  interface{} `json:"result"`
}

/*
MessageStruct describes outgoing messages from the query service
TODO: needs to generalize so that ML can handle other types of requests, not just requests based on query results
*/
type MessageML struct {
	QueryID      string      `json:"queryID"`
	Type         string      `json:"type"`
	Value        interface{} `json:"value"`
	MLAttributes interface{} `json:"mlAttributes"`
}
