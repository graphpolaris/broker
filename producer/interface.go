/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package producer

import (
	"git.science.uu.nl/graphpolaris/broker/message"
	"github.com/christinoleo/alice"
)

/*
A Producer belongs to a broker and publishes messages to a queue
*/
type BrokerProducerI interface {
	PublishMessageJsonHeaders(body *[]byte, routingKey string, headers *map[string]interface{}) BrokerProducerI
	PublishErrorMessage(err error, sessionID, routingKey string)
	PublishAnalyticsRequest(msg message.MessageFrontend, sessionID, userID, routingKey string)
	PublishFrontendMessage(msg message.MessageFrontend, sessionID, userID, routingKey string)
	PublishFrontendSuccessMessage(typeVal string, value interface{}, sessionID, userID, routingKey string)
}

/*
BrokerAliceProducer implements the producer interface
*/
type BrokerAliceProducer struct {
	producer alice.Producer
	// analyticsProducer alice.Producer
	serviceName string
}

func NewBrokerAliceProducer(producer alice.Producer, analyticsProducer alice.Producer, serviceName string) BrokerProducerI {
	return &BrokerAliceProducer{
		producer: producer,
		// analyticsProducer: analyticsProducer,
		serviceName: serviceName,
	}
}
