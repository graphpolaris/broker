package producer

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/broker/message"
)

// PublishErrorMessage publishes an error message
func (ap *BrokerAliceProducer) PublishErrorMessage(err error, sessionID, routingKey string) {
	headers := make(map[string]interface{})
	headers["sessionID"] = sessionID

	errorMessage := message.MessageFrontend{
		Type:  ap.serviceName,
		Value: err.Error(),
	}

	b, _ := json.Marshal(errorMessage)

	ap.PublishMessageJsonHeaders(&b, routingKey, &headers)
}
