package producer

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/broker/message"
)

/*
PublishAnalyticsRequest will publish the message to the queue retrieved from the key value store, with the given sessionID, to the GSA

	data: *[]byte, the data to publish
	sessionID: *string, the ID of the session
*/
func (ap *BrokerAliceProducer) PublishAnalyticsRequest(msg message.MessageFrontend, sessionID, userID, routingKey string) {
	headers := make(map[string]interface{})
	headers["sessionID"] = sessionID
	headers["userID"] = userID
	headers["type"] = ap.serviceName
	b, _ := json.Marshal(msg)
	ap.PublishMessageJsonHeaders(&b, "gsa-request", &headers)
}
