package producer

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/broker/message"
)

/*
PublishSchemaResult will publish the message to the queue retrieved from the key value store, with the given sessionID, to the UI

	data: *[]byte, the data to publish
	sessionID: *string, the ID of the session
*/
func (ap *BrokerAliceProducer) PublishFrontendMessage(msg message.MessageFrontend, sessionID, userID, routingKey string) {
	headers := make(map[string]interface{})
	headers["sessionID"] = sessionID
	headers["userID"] = userID
	headers["origin"] = ap.serviceName

	b, _ := json.Marshal(msg)

	ap.PublishMessageJsonHeaders(&b, routingKey, &headers)
}

func (ap *BrokerAliceProducer) PublishFrontendSuccessMessage(typeVal string, value interface{}, sessionID, userID, routingKey string) {
	msg := message.MessageFrontend{
		Type:   typeVal,
		Value:  value,
		Status: "success",
	}

	ap.PublishFrontendMessage(msg, sessionID, userID, routingKey)
}
