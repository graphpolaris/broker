/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package mock

import (
	"git.science.uu.nl/graphpolaris/broker"
	"git.science.uu.nl/graphpolaris/broker/consumer"
	"git.science.uu.nl/graphpolaris/broker/producer"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
)

// MockDriver is a mock broker implementing the Interface
type MockDriver struct {
	// Mock messages that are published by producers on this broker
	// Key is the routing key
	// Value is a slice of messages, in order of being sent 'first -> last'
	Messages map[string][]broker.Message
}

/*
NewMockDriver creates a new mock driver

	Returns: *MockDriver,
*/
func NewMockDriver() broker.Interface {
	return &MockDriver{
		Messages: make(map[string][]broker.Message),
	}
}

/*
CreateConsumer creates a consumer with the specified parameters (mock)

	exchangeName: string, the name of the exchange to which this consumer will bind its queue
	queueName: string, the name of the queue this consumer will consume messages from
	routingKey: string, the routingKey this consumer listens to
	Returns: An object implementing the Consumer interface
*/
func (md *MockDriver) CreateConsumer(exchangeName string, queueName string, routingKey string, routingKeyStore keyvaluestore.Interface, serviceName string) consumer.BrokerConsumerI {
	return &MockConsumer{
		broker: md,
	}
}
func (md *MockDriver) CreateConsumerWithClient(exchangeName string, queueName string, routingKey string, routingKeyStore keyvaluestore.Interface, serviceName string) (consumer.BrokerConsumerI, producer.BrokerProducerI) {
	return &MockConsumer{
			broker: md,
		}, &MockProducer{
			broker:   md,
			exchange: exchangeName,
		}
}

/*
CreateProducer creates a producer with the specified parameters (mock)

	exchangeName: string, the name of the exchange to which this producer will publish messages
	Returns: An object implementing the Producer interface
*/
func (md *MockDriver) CreateProducer(exchangeName string, serviceName string) producer.BrokerProducerI {
	return &MockProducer{
		broker:   md,
		exchange: exchangeName,
	}
}
