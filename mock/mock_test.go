/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package mock

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
Creates a mock driver and producer to test the messages

	t: *testing.T, makes go recognize this as a test
*/
func TestMockDriver(t *testing.T) {

	mockDriver := NewMockDriver().(*MockDriver)
	mockProducer := mockDriver.CreateProducer("exchangeTest", "test")

	tests := []struct {
		name       string
		routingKey string
		body       []byte
		headers    map[string]interface{}
	}{
		{
			name:       "test1",
			routingKey: "routingkey1",
			body:       []byte("body1"),
			headers:    map[string]interface{}{"key1": "value1"},
		},
		{
			name:       "test2",
			routingKey: "routingey2",
			body:       []byte("body2"),
			headers:    map[string]interface{}{"key2": "value2"},
		},
		{
			name:       "test3",
			routingKey: "routingey3",
			body:       []byte("body3"),
			headers:    map[string]interface{}{"key3": "value3"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockProducer.PublishMessageJsonHeaders(&tt.body, tt.routingKey, &tt.headers)

			assert.Equal(t, mockDriver.Messages[tt.routingKey][len(mockDriver.Messages[tt.routingKey])-1].Body, tt.body)
		})
	}
}
