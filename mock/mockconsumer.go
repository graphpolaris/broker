/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package mock

import (
	"git.science.uu.nl/graphpolaris/broker/consumer"
	"git.science.uu.nl/graphpolaris/broker/producer"
)

/*
A MockConsumer mocks the Consumer interface
*/
type MockConsumer struct {
	broker *MockDriver
}

/*
ConsumeMessages consumes messages from the broker (mock)
*/
func (mc *MockConsumer) ConsumeMessages() consumer.BrokerConsumerI {
	return mc
}

/*
SetMessageHandler sets the message handler for this consumer (mock)

	handler: func(msg *broker.Message), a functino to handle the message
*/
func (mc *MockConsumer) SetMessageHandler(handler func(msg *consumer.BrokerConsumerMessage, routingKey string, userID string, sessionID string)) consumer.BrokerConsumerI {
	return mc
}
func (mc *MockConsumer) GetClientProducer() producer.BrokerProducerI {
	return nil
}
func (mc *MockConsumer) AddClientProducer(producer producer.BrokerProducerI) {
}
