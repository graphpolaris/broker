package consumer

import (
	"git.science.uu.nl/graphpolaris/broker/producer"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/christinoleo/alice"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog/log"
)

/*
A Consumer belongs to a broker and consumes messages from a queue
*/
type BrokerConsumerI interface {
	ConsumeMessages() BrokerConsumerI
	// (msg *broker.Message, routingKey string, userID string, sessionID string)
	SetMessageHandler(handler func(msg *BrokerConsumerMessage, routingKey string, userID string, sessionID string)) BrokerConsumerI
	GetClientProducer() producer.BrokerProducerI
	AddClientProducer(producer producer.BrokerProducerI)
}

/*
BrokerConsumerMessage describes the message to be handled by the message consumer
*/
type BrokerConsumerMessage struct {
	Headers map[string]interface{}
	Body    []byte
}

/*
BrokerAliceConsumer implements the consumer interface
*/
type BrokerAliceConsumer struct {
	consumer        alice.Consumer
	producer        producer.BrokerProducerI
	serviceName     string
	messageHandler  func(msg *BrokerConsumerMessage, routingKey string, userID string, sessionID string)
	routingKeyStore keyvaluestore.Interface
}

func (ac *BrokerAliceConsumer) GetClientProducer() producer.BrokerProducerI {
	return ac.producer
}

func NewBrokerAliceConsumer(consumer alice.Consumer, routingKeyStore keyvaluestore.Interface, serviceName string) BrokerConsumerI {
	return &BrokerAliceConsumer{
		consumer:        consumer,
		routingKeyStore: routingKeyStore,
		serviceName:     serviceName,
	}
}

func (ac *BrokerAliceConsumer) AddClientProducer(producer producer.BrokerProducerI) {
	ac.producer = producer
}

/*
ConsumeMessages starts the consumption of messages by this consumer. The consumer is started in a new goroutine. Every incoming message is passed to the handler on a new goroutine
*/
func (ac *BrokerAliceConsumer) ConsumeMessages() BrokerConsumerI {
	go ac.consumer.ConsumeMessages(nil, false, ac.handleMessage)
	return ac
}

/*
HandleMessage takes a message, gives it to the handler, and acknowledges that it's been handled

	msg: amqp.Delivery, The body of the message being sent
*/
func (ac *BrokerAliceConsumer) handleMessage(msg amqp.Delivery) {
	// Convert the incoming message to our format
	m := &BrokerConsumerMessage{
		Headers: (map[string]interface{})(msg.Headers),
		Body:    msg.Body,
	}

	log.Trace().Str("type", "consumer").Str("routingKey", msg.RoutingKey).Str("ConsumerTag", msg.ConsumerTag).Str("Exchange", msg.Exchange).Str("RoutingKey", msg.RoutingKey).Msg("received message from rabbit")
	ac.HandleRouting(m)

	// Acknowledge the message
	msg.Ack(false)
}

/*
SetMessageHandler sets the message handler this consumer will use. Any incoming message will be transformed and then passed to this handler

	handler: func(msg *Message), the method which should handle incoming messages for this consumer
*/
func (ac *BrokerAliceConsumer) SetMessageHandler(handler func(msg *BrokerConsumerMessage, routingKey string, userID string, sessionID string)) BrokerConsumerI {
	ac.messageHandler = handler
	return ac
}
