package consumer

import (
	"context"
	"errors"
	"fmt"
	"time"

	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/rs/zerolog/log"
)

func (ac *BrokerAliceConsumer) HandleRouting(msg *BrokerConsumerMessage) {
	// Grab the userID and sessionID from the message headers

	log.Trace().Msg("Consuming message! 1")
	userID, sessionID, err := getHeaders(msg)
	if err != nil {
		log.Error().Str("userID", userID).Str("sessionID", sessionID).Msg(err.Error())
		return
	}

	// Get the routingKey to this client from Redis
	routingKey, err := ac.getRoutingKey(sessionID)
	if err != nil {
		log.Error().Str("userID", userID).Str("sessionID", sessionID).Msg(err.Error())
		return
	}
	log.Trace().Str("userID", userID).Str("sessionID", sessionID).Str("routingKey", routingKey).Msg("Consuming message! 2")

	// Defer a panic intercepting function
	defer func(sessionID string) {
		if err := recover(); err != nil {
			err, ok := err.(error)
			if !ok {
				err = fmt.Errorf("%v", err)
			}

			log.Panic().AnErr("err", err).Msg("Error in message handler")

			// Send error message
			if ac.producer != nil {
				ac.producer.PublishErrorMessage(err, sessionID, routingKey)
			} else {
				log.Warn().Msg("No producer available to send error message")
			}
		}
	}(sessionID)

	// Call the consumer message handler
	ac.messageHandler(msg, routingKey, userID, sessionID)

}

// getHeaders gets the userID and sessionID values from the message headers
func getHeaders(msg *BrokerConsumerMessage) (userID string, sessionID string, err error) {
	userID, ok := msg.Headers["userID"].(string)
	if !ok {
		err = errors.New("missing userID in headers")
		return
	}

	sessionID, ok = msg.Headers["sessionID"].(string)
	if !ok {
		err = errors.New("missing sessionID in headers")
		return
	}

	return
}

// getRoutingKey gets the routingKey for this session from Redis
func (ac *BrokerAliceConsumer) getRoutingKey(sessionID string) (routingKey string, err error) {
	getClientQueueIDContext, cancelGetClientQueueID := context.WithTimeout(context.Background(), time.Second*10)
	defer cancelGetClientQueueID()
	routingKeyVal, err := ac.routingKeyStore.Get(getClientQueueIDContext, fmt.Sprintf("routing %s", sessionID), keyvaluestore.String)
	if err != nil {
		return
	}

	routingKey = routingKeyVal.(string)

	// Check if there is an actual queue, if there is none don't publish a message
	if routingKey == "" {
		log.Warn().Str("sessionID", sessionID).Msg("Unable to find routingKey in redis, waiting 2s")
		time.Sleep(2 * time.Second) // TODO: make so each service creates if not exist
		routingKeyVal, err = ac.routingKeyStore.Get(getClientQueueIDContext, sessionID, keyvaluestore.String)
		if err != nil {
			return
		}

		routingKey = routingKeyVal.(string)
		if routingKey == "" {
			err = errors.New(fmt.Sprintf("no routing key found for id %s", sessionID))
			return
		}
	}

	log.Trace().Str("sessionID", sessionID).Str("routingKey", routingKey).Msg("found routingKey for session")

	return
}
